Cel:
----

Aplikacja słóży do ogarniania bałaganu w szafie. Jeśli ktoś ma mało miejsca w
garderobie a dużo rzeczy to przestaje ogarniać co właściwie ma. W aplikacji
można przechowywać zdjęcia wszystkich ubrań i dodatków jakie posiadamy w
poukładany sposób z podziałem na kategorie. Można również przechowywać gotowe
zestawy ubrań z podziałem na konkretne okazje. Użytkownik aplikacji może w łatwy
sposób przejrzeć całą zawartość szafy co pomoże mu w doborze odpowiedniego
ubioru.

Użytkownicy: Każdy kto ma bałagan w szafie a chciałby widzieć swoje rzeczy
ładnie posortowane żeby łatwiej dobierać sobie części garderoby pasujące do
siebie.

What the app would look like:
It has a left hand side(?)menu so the user can select clothes by category:

* Dresses
* Tops
* Shirts&Blouses
* Cardigans&Jumpers
* Jackets
* Coates
* Shorts
* Trousers
* Jeans
* Skirts
* Shoes
* Accessories
* Sportswear
* Miscellaneous

It shows 3 items per row (?). In top menu there's a colour filter and occasion filter.
It also has ready-made sets of clothes that user can prepare by themself for instance:
Office, Holiday, Party, Casual, Date ...

Jak zaprojektować menu? Czy zrobić po lewej stronie jak powyżej? H&M ma takie
menu i jak się kliknie np w tops to pojawia się podmenu(nadal po lewej stronie):
krótki/długi rękaw itp...ale też pojawia się na górze filt category i można
rozwinąć.

Czy zrobić też coś takiego czy olać? To category na górze byłoby dobre do
gotowych zestawów. A może w ogóle na górze zrobić taki filtr occasion i
np: office, party, holiday, casual. Gotowe zestawy przenieść na kolejne sprinty.
Można zrobić kreator zestawów żeby je samemu tworzyć.

What the app whould do:
Possibility of uploading photos of clothes and tag them:dress, colour, occasion
Possibility of viewing the clothes and filtering by colour, category, occasion.
MVP:
Image upload - form with tags: category, colour, description
Categories, colours, occasions filters on the website
View items
Edit items
przy wgrywaniu zdjęcia będzie trzeba określić co to jest: sukienka, buzka etc, jaki ma kolor, na jaką okazję z czego obowiązkowo co to jest i kolor,
okazję można dopasować później.
W kolejnych iteracjach możba dać userowi możliwość tworzenia własnych kategorii ubrań i okazji, może też kolorów.
Czy zacząć implementację od Userów i loginu?

