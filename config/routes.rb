Rails.application.routes.draw do
  resources :cloth_items do
    member do
      get :photo
    end
  end

  devise_for :accounts, controllers: {
    registrations: 'account/registrations',
  }
  get 'demo', to: 'demo#index'
  root 'demo#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
