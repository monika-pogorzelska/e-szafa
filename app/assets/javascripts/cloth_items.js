$(document).on('turbolinks:load', function() {
  $("a.cloth_item_delete").click(function(event){
    var id = $(this).data('cloth-item-id');
    cloth_item_delete_confirmation(id);
    event.preventDefault();
  });

  $("a.cancel_cloth_item_deletion").click(function(event){
    event.preventDefault();
    var id = $(this).data('cloth-item-id');
    hide_cloth_item_deletion(id);
  });
});

function cloth_item_delete_confirmation(id) {
  $(`.back[data-cloth-item-id='${id}']`).fadeIn(300);
}

function hide_cloth_item_deletion(id) {
  $(`.back[data-cloth-item-id='${id}']`).fadeOut(200);
}

