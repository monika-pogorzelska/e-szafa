class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_user
    current_account.user if current_account
  end

  def user_signed_in?
    account_signed_in?
  end

end
