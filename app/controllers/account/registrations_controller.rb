class Account::RegistrationsController < Devise::RegistrationsController
  def new
    super do |account|
      account.build_user name: ''
    end
  end

  protected

  def sign_up_params
    params.require(:account).permit(:email, :password, :password_confirmation,
                                    {user_attributes: [:name]})
  end
end
