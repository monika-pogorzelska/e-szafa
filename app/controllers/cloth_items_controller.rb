class ClothItemsController < ApplicationController
  require 'action_view'
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::TextHelper
  before_action :authenticate_account!
  before_action :authorize, only: [:show, :edit, :update, :destroy]

  def index
    session[:category] = params[:category] if params[:category]
    session[:category] = nil if params[:destroy_category]
    session[:color] ||= []
    if params[:color]
      session[:color] << params[:color] unless session[:color].include? params[:color]
    end
    session[:color] = [] if params[:destroy_all_colors]
    session[:color].delete(params[:destroy_color]) if params[:destroy_color]
    @cloth_items = current_user.cloth_items.paginate(page: params[:page],
                                                     per_page: 24)

    @cloth_items = @cloth_items.of_category(session[:category]) if session[:category]
    @cloth_items = @cloth_items.of_color(session[:color]) if session[:color].any?
    @active_color = session[:color]
    @active_category = session[:category]
  end

  def show
    @cloth_item = ClothItem.find(params[:id])
  end

  def new
    @cloth_item = ClothItem.new
    @tags = Tag.all
  end

  def create
    @tags = Tag.all

    params_not_empty? or return

    @cloth_item = current_user.cloth_items.build(basic_params)

    ActiveRecord::Base.transaction do
      @cloth_item.update_photo!(params[:cloth_item][:photo], dir: photos_dir)
      if params[:cloth_item][:tags]
        entered_tags = params[:cloth_item][:tags].split(",")
        @cloth_item.add_tags(entered_tags)
      end
      @cloth_item.save!
      flash[:success] = 'Cloth item has been added'
    end
    redirect_to cloth_item_path(@cloth_item)
    rescue ClothItem::PhotoError => e
      flash[:danger] = e.message
      render :new
    rescue ActiveRecord::ActiveRecordError => e
      render :new
  end

  def edit
    @cloth_item = ClothItem.find(params[:id])
    @tags = Tag.all
  end

  def update
    @cloth_item = ClothItem.find(params[:id])
    @cloth_item.update!(basic_params)
    if params[:cloth_item][:photo]
      @cloth_item.update_photo!(params[:cloth_item][:photo], dir: photos_dir)
    end
    entered_tags = params[:cloth_item][:tags].split(",")
    @cloth_item.update_tags!(entered_tags)
    redirect_to cloth_item_path(@cloth_item)
    flash[:success] = 'Cloth item has been updated'
    rescue ClothItem::PhotoError => e
      flash[:danger] = e.message
      render :edit
    rescue ActiveRecord::ActiveRecordError => e
      flash[:danger] = 'Failed to update cloth item'
      render :edit
  end

  def destroy
    ActiveRecord::Base.transaction do
      cloth_item = ClothItem.find(params[:id])
      cloth_item.destroy!
      cloth_item.delete_photo(dir: photos_dir)
      flash[:danger] = "Cloth item has been deleted"
      redirect_back(fallback_location: cloth_items_path)
    end
    rescue ActiveRecord::ActiveRecordError => e
    rescue SystemCallError => e
      flash[:danger] = "Failed to delete cloth item"
      redirect_to cloth_items_path
  end

  def photo
    cloth_item = ClothItem.find(params[:id])
    if cloth_item.user != current_user
      head 401
      return
    end
    file_path = photos_dir.join(cloth_item.photo).to_s
    file_name = cloth_item.photo
    send_file(file_path, filename: file_name )
  end

  private

    def basic_params
      params.require(:cloth_item).permit(:category_id, :color_id)
    end

    def params_not_empty?
      return true if params[:cloth_item]
      @cloth_item = current_user.cloth_items.build
      @cloth_item.validate
      render :new
      return false
    end

    def photos_dir
      Rails.root.join("uploads/photos", current_user.id.to_s)
    end

    def authorize
      cloth_item = ClothItem.find(params[:id])
      unless cloth_item.user == current_user
        redirect_to(root_url)
        flash[:danger] = "this cloth item doesn't belong to you"
      end
    end
end


