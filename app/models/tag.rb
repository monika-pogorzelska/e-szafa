class Tag < ApplicationRecord
  has_and_belongs_to_many :cloth_items
  before_save { self.name = name.downcase }
  validates :name, uniqueness: { case_sensitive: false }
end
