class ClothItem < ApplicationRecord
  include ActionView::Helpers::NumberHelper
  MAX_SIZE = 1.megabytes
  ALLOWED_TYPES = %w[image/jpeg image/gif image/png]

  belongs_to :user
  belongs_to :color, optional: true
  belongs_to :category, optional: true
  has_and_belongs_to_many :tags

  validates_presence_of :category, message: "must be selected"
  validates_presence_of :color, message: "must be selected"
  validates_presence_of :photo, message: "must be selected"

  scope :of_category, -> (name) {
    category = Category.find_by(name: name)
    where category: category
  }

  scope :of_color, -> (names) {
    colors = Color.where(name: names)
    where color: colors
  }

  def add_tags(tag_names)
    tag_names.each { |tag_name| add_tag(tag_name) }
  end

  def update_tags!(tag_names)
    ActiveRecord::Base.transaction do
      tags.destroy_all
      add_tags(tag_names)
      save!
    end
  end

  def add_tag(tag_name)
    existing_tag = Tag.find_by(name: tag_name)
    if existing_tag
      tags << existing_tag
    else
      tags.build(name: tag_name)
    end
  end

  def update_photo!(uploaded_file, dir:)
    return unless uploaded_file
    check_photo_size(uploaded_file)
    check_photo_type(uploaded_file)
    delete_photo(dir: dir) if self.photo
    update! photo: photo_name(uploaded_file)
    save_photo(dir, photo, uploaded_file)
  end

  def delete_photo(dir:)
    photo_path = File.join(dir, photo)
    File.delete(photo_path)
  end

  private

  def photo_name(uploaded_file)
    ext = File.extname(uploaded_file.original_filename)
    name = Time.now.to_f.to_s
    return "#{name}#{ext}"
  end

  def check_photo_size(uploaded_file)
    return if uploaded_file.size <= MAX_SIZE
    max_size = number_to_human_size(MAX_SIZE)
    raise PhotoError, "Photo size can't be bigger than #{max_size}"
  end

  def check_photo_type(uploaded_file)
    return if ALLOWED_TYPES.include? uploaded_file.content_type
    allowed_types = ALLOWED_TYPES.join(', ')
    raise PhotoError, "Only photo formats allowed: #{allowed_types}"
  end

  def save_photo(dir, photo_filename, uploaded_file)
    Dir.mkdir dir unless Dir.exist? dir
    photo_path = File.join(dir, photo_filename)
    File.open(photo_path, 'wb') do |file|
      file.write(uploaded_file.read)
    end
  end

end

class ClothItem::PhotoError < StandardError; end
