class User < ApplicationRecord
  has_one :account, dependent: :destroy
  has_many :cloth_items, dependent: :destroy
  # before_save { self.name = name.downcase }
  VALID_NAME_REGEX = /\A[a-z]+[\w+\-.]+\z/i
  validates :name, length: {minimum: 3, maximum: 32},
                   format: { with: VALID_NAME_REGEX },
                   uniqueness: { case_sensitive: false }
end
