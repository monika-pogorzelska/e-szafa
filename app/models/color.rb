class Color < ApplicationRecord
  has_many :cloth_items
  validates :name, uniqueness: { case_sensitive: false }
end
