require 'test_helper'

class FilterClothItemsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
     @account = accounts(:littlefinger_account)
     @category = categories(:dress)
     @trousers = cloth_items(:trousers)
     @color_2 = colors(:red)
     @color_3 = colors(:green)
     user = @account.user
     user.cloth_items.create(category_id: @category.id,
                              color_id: @color_2.id,
                              photo: "picture")
     user.cloth_items.create(category_id: @category.id,
                              color_id: @color_3.id,
                              photo: "picture2")
  end

  test "should filter cloth items" do
     sign_in @account
     get cloth_items_path, params: { category: 'Trousers' }
     assert_select "div.gallery_img", count: 1
     get cloth_items_path, params: { destroy_category: true }
     assert_select "div.gallery_img", count: 3
     get cloth_items_path, params: { color: 'red' }
     assert_select "div.gallery_img", count: 1
     get cloth_items_path, params: { destroy_color: 'red' }
     assert_select "div.gallery_img", count: 3
     get cloth_items_path, params: { category: 'Trousers', color: 'violet' }
     assert_select "a.btn-info", count: 2
     assert_select "a.btn-danger", count: 1
     assert_select "div.gallery_img", count: 1
     get cloth_items_path, params: { destroy_category: true, destroy_color: 'violet' }
     get cloth_items_path, params: { category: 'Dress', color: 'red'}
     get cloth_items_path, params: {color: 'green'}
     assert_select "div.gallery_img", count: 2
     get cloth_items_path, params: { destroy_category: true, destroy_color: 'red' }
     get cloth_items_path, params: {destroy_color: 'green'}
     assert_match "Show all", response.body
  end

  test "should remember filter" do
    sign_in @account
    get cloth_items_path, params: { category: 'Trousers' }
    assert_select "div.gallery_img", count: 1
    get cloth_items_path
    assert_select "div.gallery_img", count: 1
  end
end
