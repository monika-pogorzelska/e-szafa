require 'test_helper'

class UpdateTagsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    load "#{Rails.root}/db/seeds.rb"
    @account = accounts(:littlefinger_account)
    @user = @account.user
  end

  test "should update tags" do
    src = Rails.root.join("test/fixtures/files/fancypants.jpeg")
    photo_dir = Rails.root.join("uploads/photos", @user.id.to_s)
    Dir.mkdir photo_dir unless Dir.exist? photo_dir
    FileUtils.cp(src, photo_dir)
    cloth_item = cloth_items(:trousers)
    sign_in @account
    get edit_cloth_item_path(cloth_item)
    assert_select "form#edit_cloth_item_#{cloth_item.id}"
    params = { cloth_item: { tags: 'holiday' } }
    patch cloth_item_path(cloth_item), params: params
    assert_not flash.empty?
    assert_redirected_to cloth_item
    cloth_item.reload
    cloth_item = ClothItem.find_by(id: assigns(:cloth_item).id )
    assert_equal ["holiday"], cloth_item.tags.map(&:name)
    assert_equal 1, cloth_item.tags.size
  end

end
