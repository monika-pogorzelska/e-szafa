require 'test_helper'

class AddingClothItemTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    load "#{Rails.root}/db/seeds.rb"
    @user = accounts(:littlefinger_account)
  end

  test "should add cloth item with tags" do
    sign_in @user
    category = categories(:dress)
    color = colors(:violet)
    picture = fixture_file_upload('test/fixtures/files/ruby.jpg', 'image/jpeg')
    tag_1 = tags(:casual)
    tag_2 = tags(:work)
    params = {
      cloth_item: {
        category_id: category.id,
        color_id: color.id,
        photo: picture,
        tags: "#{tag_1.name},#{tag_2.name},cool,fun"
      }
    }
    post cloth_items_path, params: params
    cloth_item = ClothItem.find_by(id: assigns(:cloth_item).id )
    assert_redirected_to cloth_item_path(cloth_item)
    assert_equal ["casual", "work", "cool", "fun"], cloth_item.tags.map(&:name)
    assert_equal 7, Tag.all.count
    photo_path = Rails.root.join("uploads/photos", @user.id.to_s, cloth_item.photo)
    File.exist? photo_path
  end
end
