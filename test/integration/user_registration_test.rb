require 'test_helper'

class UserRegistrationTest < ActionDispatch::IntegrationTest
  test "user registration form" do
    get new_account_registration_path
    assert_select 'form input', 6
    assert_select 'label', 'Name'
    assert_select 'label', 'Email'
    assert_select 'label', 'Password'
    assert_select 'label', 'Password confirmation'
  end

  test "validation of user name in registration" do
    params_with_blank_name = {
      params: {
        account: {
          email: 'asd@asd.pl',
          password: '123456',
          password_confirmation: '123456',
          user_attributes: { name: '' }
        }
      }
    }
    assert_no_difference 'User.count' do
      post account_registration_path, params_with_blank_name
    end
  end

  test "user registration" do
    assert_difference 'User.count', 1 do
      post account_registration_path,
        params: {
          account: {
            email: 'asd@asd.pl',
            password: '123456',
            password_confirmation: '123456',
            user_attributes: { name: 'asdfgh' }
          }
        }
    end
  end
end
