require 'test_helper'

class ClothItemTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    color = colors(:violet)
    @category = categories(:dress)
    @color_1 = colors(:violet)
    @color_2 = colors(:red)
    @color_3 = colors(:green)
    @casual = tags(:casual)
    @work = tags(:work)
    @cloth_item = ClothItem.new(user: @user, color: color, category: @category, photo: "photo")
    @cloth_item.tags << @work
    @cloth_item.tags << @casual
  end

  test "should be valid" do
    assert @cloth_item.valid?
    assert @cloth_item.category
    assert @cloth_item.color
    assert @cloth_item.user
    assert_includes @cloth_item.tags, @casual
    assert_includes @cloth_item.tags, @work
  end

  test "user id should be present" do
    @cloth_item.user = nil
    assert_not @cloth_item.valid?
  end

  test "color id should be present" do
    @cloth_item.color = nil
    assert_not @cloth_item.valid?
  end

  test "category id should be present" do
    @cloth_item.category = nil
    assert_not @cloth_item.valid?
  end

  test "filter by color" do
    @user.cloth_items.create(category: @category,
                              color: @color_1,
                              photo: "picture1")
    @user.cloth_items.create(category: @category,
                              color: @color_2,
                              photo: "picture2")
    @user.cloth_items.create(category: @category,
                              color: @color_3,
                              photo: "picture3")
    items = @user.cloth_items.of_color(['violet', 'red'])
    assert_equal 2, items.size
    assert_includes items.map { |item| item.color.name }, 'red'
    assert_includes items.map { |item| item.color.name }, 'violet'
  end
end
