require 'test_helper'

class ColorTest < ActiveSupport::TestCase
  test "color should be unique" do
    assert_raise do
      Color.create!(name: 'violet')
    end
  end
end
