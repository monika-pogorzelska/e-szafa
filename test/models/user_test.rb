require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "ExampleUser")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = " "
    @user.valid?
    assert_includes(@user.errors.full_messages, "Name is invalid")
  end

  test "name should not be too long" do
    @user.name = "a" * 33
    @user.valid?
    assert_includes(@user.errors.full_messages,
                    "Name is too long (maximum is 32 characters)")
  end

  test "name should not be too short" do
    @user.name = "aa"
    @user.valid?
    assert_includes(@user.errors.full_messages,
                    "Name is too short (minimum is 3 characters)")
  end

  test "name should has valid format" do
    valid_names = %w[user_example.com USER-foo A_US first1984 alice66]
    valid_names.each do |valid_name|
      user = User.new(name: valid_name)
      assert user.valid?, "#{valid_name.inspect} should be valid"
    end
  end

  test "name validation should reject invalid names" do
    invalid_names = %w[4444 monika@wp.pl trele,morele 3dwajeden %$#! waz!]
    invalid_names.each do |invalid_name|
      user = User.new(name: invalid_name)
      user.valid?
      assert_includes(user.errors.full_messages, "Name is invalid")
    end
  end

  test "name should be unique" do
    duplicate_user = @user.dup
    @user.save
    duplicate_user.name = "EXAMPLEUSER"
    assert_not duplicate_user.valid?
    assert_includes(duplicate_user.errors.full_messages, "Name has already been taken")
  end

  test "associated account should be destroyed" do
    @user.save
    @user.create_account!(email: "kaka@wp.pl", password: "sratatata")
    assert_difference 'Account.count', -1 do
      @user.destroy
    end
  end
end