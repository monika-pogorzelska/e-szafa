require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test "tags should be unique" do
    assert_raise do
      Tag.create!(name: 'work')
    end
    assert_raise do
      Tag.create!(name: 'WORK')
    end
  end
end
