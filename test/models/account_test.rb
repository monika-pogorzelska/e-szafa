require 'test_helper'

class AccountTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @account = @user.build_account(email: "michael@wp.pl",
                                    password: "123456")
  end

  test "should be valid" do
    assert @account.valid?
  end

  test "user id should be present" do
    @account.user = nil
    assert_not @account.valid?
  end

end
