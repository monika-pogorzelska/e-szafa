require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "category should be unique" do
    assert_raise do
      Category.create!(name: 'dress')
    end
  end
end
