require 'test_helper'

class ClothItemsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  def setup
    @account = accounts(:littlefinger_account)
    @account_2 = accounts(:aria_account)
    @trousers = cloth_items(:trousers)
    @dress = cloth_items(:dress)
    @category = categories(:dress)
    @color = colors(:violet)
  end

  test "should get index" do
    sign_in @account
    get cloth_items_path
    assert_response :success
    image_trousers = photo_cloth_item_path(@trousers)
    assert_select "div[style = \"background-image: url('#{image_trousers}');\"]"
    assert_select "div.gallery_img"
    image_dress = photo_cloth_item_path(@dress)
    assert_select "div[style = \"background-image: url('#{image_dress}');\"]", false
  end

  test "should have pagination" do
    user = @account.user
    30.times do |n|
      user.cloth_items.create(category_id: @category.id,
                              color_id: @color.id,
                              photo: "picture_#{n}")
    end
    sign_in @account
    get cloth_items_path
    assert_response :success
    assert_select 'div.pagination'
    assert_select 'a[href=?]', '/cloth_items?page=2', text: '2'
  end

  test "should get new" do
    sign_in @account
    get new_cloth_item_path
    assert_response :success
  end

  test "should redirect show for wrong cloth item" do
    sign_in @account
    get cloth_item_path(@trousers)
    assert_response :success
    get cloth_item_path(@dress)
    assert_redirected_to root_path
  end

  test "should redirect destroy for wrong cloth item" do
    sign_in @account
    assert_no_difference 'ClothItem.count' do
      delete cloth_item_path(@dress)
    end
    assert_redirected_to root_path
  end

  test "should delete cloth item" do
    sign_in @account_2
    user = @account_2.user
    user.cloth_items.create(category_id: @category.id,
                            color_id: @color.id,
                            photo: "dress2.png")
    src = Rails.root.join("test/fixtures/files/dress2.png")
    photo_dir = Rails.root.join("uploads/photos", user.id.to_s)
    Dir.mkdir photo_dir unless Dir.exist? photo_dir
    FileUtils.cp(src, photo_dir)
    photo_path = File.join(photo_dir, "dress2.png")
    assert File.exist? photo_path
    cloth_item = user.cloth_items.first
    assert_difference 'ClothItem.count', -1 do
      delete cloth_item_path(cloth_item)
    end
    assert_not File.exist? photo_path
  end
end
