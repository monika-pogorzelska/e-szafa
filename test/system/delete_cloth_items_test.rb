require "application_system_test_case"

class DeleteClothItemsTest < ApplicationSystemTestCase
  setup do
    load "#{Rails.root}/db/seeds.rb"
    account = accounts(:littlefinger_account)
    @user = account.user
    visit new_account_session_path
    fill_in "Email", with: account.email
    fill_in "Password", with: "password"
    click_button "Log in"
  end

  test "delete cloth item" do
    category = categories(:dress)
    color = colors(:violet)
    cloth_item = @user.cloth_items.create(category_id: category.id,
                                         color_id: color.id,
                                         photo: "dress2.png")
    src = Rails.root.join("test/fixtures/files/dress2.png")
    photo_dir = Rails.root.join("uploads/photos", @user.id.to_s)
    Dir.mkdir photo_dir unless Dir.exist? photo_dir
    FileUtils.cp(src, photo_dir)
    visit cloth_items_path
    within("a[data-cloth-item-id='#{cloth_item.id}']") do
      find("span[class='glyphicon glyphicon-trash']").click
    end
    find("a[data-cloth-item-id='#{cloth_item.id}'][data-method='delete']").click
    assert_text 'Cloth item has been deleted'
    photo_path = File.join(photo_dir, "dress2.png")
    assert_not File.exist? photo_path
  end

end
