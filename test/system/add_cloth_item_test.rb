require "application_system_test_case"

class AddClothItemTest < ApplicationSystemTestCase
  setup do
    load "#{Rails.root}/db/seeds.rb"
    visit new_account_session_path
    fill_in "Email", with: "sansa@got.com"
    fill_in "Password", with: "password"
    click_button "Log in"
  end

  test "add cloth item" do

    visit new_cloth_item_path

    choose ("Dresses")
    choose ("white")
    attach_file "cloth_item_photo", Rails.root.join('test/fixtures/files/ruby.jpg')
    click_button 'Add Cloth Item'

    assert_text "Cloth item has been added"
  end

  test "prevent too big photos from being uploaded" do

    visit new_cloth_item_path

    choose "Dresses"
    choose "white"
    attach_file "cloth_item_photo", Rails.root.join('test/fixtures/files/flower.jpg')
    click_button 'Add Cloth Item'

    assert_text "Photo size can't be bigger than 1 MB"
  end

  test "prevent photos in format other than jpeg, png, gif from being uploaded" do
    visit new_cloth_item_path

    choose "Dresses"
    choose "white"
    attach_file "cloth_item_photo", Rails.root.join('test/fixtures/files/rubyby.txt')
    click_button 'Add Cloth Item'

    assert_text 'Only photo formats allowed: image/jpeg, image/gif, image/png'

  end

end
