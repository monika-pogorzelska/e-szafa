require "application_system_test_case"

class RegistrationsTest < ApplicationSystemTestCase
  test "viewing the sign up page" do
    visit new_account_registration_path
    assert_selector "h2", text: "Sign up"
  end

  test "new user registration" do
    visit new_account_registration_path
    fill_in "Name", with: "Monika"
    fill_in "Email", with: "moniczka@wp.pl"
    fill_in "Password", with: "123456"
    fill_in "Password confirmation", with: "123456"
    click_button "Sign up"
    assert_text "Welcome! You have signed up successfully."
  end
end
