require "application_system_test_case"

class UpdateClothItemsTest < ApplicationSystemTestCase
  setup do
    load "#{Rails.root}/db/seeds.rb"
    account = accounts(:littlefinger_account)
    @user = account.user
    visit new_account_session_path
    fill_in "Email", with: account.email
    fill_in "Password", with: "password"
    click_button "Log in"
  end

  test "update_cloth_item" do
    category = categories(:dress)
    color = colors(:violet)
    cloth_item = @user.cloth_items.create(category_id: category.id,
                                         color_id: color.id,
                                         photo: "dress2.png")
    photo_show = photo_cloth_item_path(cloth_item)
    photo_edit = edit_cloth_item_path(cloth_item)
    src = Rails.root.join("test/fixtures/files/dress2.png")
    photo_dir = Rails.root.join("uploads/photos", @user.id.to_s)
    Dir.mkdir photo_dir unless Dir.exist? photo_dir
    FileUtils.cp(src, photo_dir)
    visit cloth_items_path
    find("div[style = \"background-image: url('#{photo_show}');\"]").click
    find("a[href = '#{photo_edit}']").click
    choose ("Tops")
    choose ("pink")
    attach_file "cloth_item_photo", Rails.root.join('test/fixtures/files/dress333.png')
    click_button 'Save changes'
    cloth_item.reload
    page.has_content?('Category:')
    assert_equal "pink", cloth_item.color.name
    assert_equal cloth_item.category.name, "Tops"
    page.has_xpath?("//image[@src = '#{photo_cloth_item_path(cloth_item)}']")
    assert_not_equal cloth_item.photo, "dress2.png"
    photo_path = File.join(photo_dir, "dress2.png")
    assert_not File.exist? photo_path
  end
end
