require "application_system_test_case"

class LoginsTest < ApplicationSystemTestCase
 test "viewing the login page" do
    visit new_account_session_path
    assert_selector "h2", text: "Log in"
  end

  test "user login" do
    visit new_account_session_path
    fill_in "Email", with: "lovetoplot@got.com"
    fill_in "Password", with: "password"
    click_button "Log in"
    assert_text "Signed in successfully."
  end
end

