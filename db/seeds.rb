# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
categories = %w[
  Dresses
  Tops
  Shirts&Blouses
  Cardigans&Jumpers
  Jackets
  Coates
  Shorts
  Trousers
  Jeans
  Skirts
  Shoes
  Accessories
  Sportswear
  Miscellaneous
]

categories.each do |category|
  Category.create(name: "#{category}")
end

colors = %w[
  black
  white
  grey
  brown
  violet
  blue
  green
  yellow
  red
  pink
  orange
  multicolor
]

colors.each do |color|
  Color.create(name: "#{color}")
end

tags = %w[
  casual
  work
  party
  holidays
  sport
]

tags.each do |tag|
  Tag.create(name: "#{tag}")
end

