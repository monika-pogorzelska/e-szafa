class ClothItemsTagsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :cloth_items_tags, id: false do |t|
      t.belongs_to :tag, index: true
      t.belongs_to :cloth_item, index: true
    end
  end
end
