class CreateColors < ActiveRecord::Migration[5.1]
  def change
    create_table :colors do |t|
      t.string :name

      t.timestamps
    end
    add_column :cloth_items, :color_id, :integer, null: false, default: 0
    add_foreign_key :cloth_items, :colors
    add_index :cloth_items, :color_id
  end
end
