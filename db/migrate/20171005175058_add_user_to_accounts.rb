class AddUserToAccounts < ActiveRecord::Migration[5.1]
  def change
    add_column :accounts, :user_id, :integer, null: false, default: 0
    add_index :accounts, :user_id, unique: true
    add_foreign_key :accounts, :users
  end
end
