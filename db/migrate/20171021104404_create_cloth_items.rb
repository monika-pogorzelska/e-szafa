class CreateClothItems < ActiveRecord::Migration[5.1]
  def change
    create_table :cloth_items do |t|
      t.string :photo

      t.timestamps
    end
  end
end