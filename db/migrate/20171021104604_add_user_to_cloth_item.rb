class AddUserToClothItem < ActiveRecord::Migration[5.1]
  def change
    add_column :cloth_items, :user_id, :integer, null: false, default: 0
    add_index :cloth_items, :user_id
    add_foreign_key :cloth_items, :users
  end
end
