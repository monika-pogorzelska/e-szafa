class AddIndexToColorsCategoriesTags < ActiveRecord::Migration[5.1]
  def change
    add_index :categories, :name, unique: true
    add_index :colors, :name, unique: true
    add_index :tags, :name, unique: true
  end
end
