photo = %w[dress1.png dress2.png dress3.png dress5.jpg
           dress6.png dress7.png dress9.jpg dress10.png]
user = User.find(1)

40.times do |n|
  cloth_item = user.cloth_items.create(
                                      id: n,
                                      photo: "#{photo[rand(7)]}",
                                      user_id: 1,
                                      color_id: rand(11),
                                      category_id: 1
                                      )
end

user = User.find(1)
tag = %w[casual work]
user.cloth_items.each do |cloth_item|
  cloth_item << "#{tag}"
end

user = User.find(1)
tags = %w[casual work]
user.cloth_items.each do |cloth_item|
  cloth_item.add_tags(tags)
  cloth_item.save
end
